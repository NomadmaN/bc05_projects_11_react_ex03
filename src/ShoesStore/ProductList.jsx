import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class ProductList extends Component {
    renderProductList = () => {
        // always have return at start of function
        // props = from parent class, productList defined in parent class for <ProductList> component
        return this.props.productList.map((item) => {
            return <ProductItem dataProduct={item} detailButton={this.props.viewDetail} cartButton={this.props.addToCart}></ProductItem>
        })
    }
  render() {
    return (
      <div className='row'>
        {this.renderProductList()}
      </div>
    )
  }
}
