import React, { Component } from 'react'

export default class ModalDetail extends Component {
  render() {
    let {image,name,price,description,quantity} = this.props.shoesInfo;
    return (
      <div>
        {/* Modal */}
        <div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content" style={{width: '600px', maxWidth: '600px'}}>
              <div className="modal-header">
                <h5 className="modal-title" style={{fontWeight: 'bold', fontSize: '30px'}}>PRODUCT DETAIL</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className='row py-5'>
                  <div className='col-12'>
                      <h4 style={{backgroundColor:'#F3EFE0'}}>{name}</h4>
                      <img className='pt-1' style={{width: '300px'}} src={image} alt="" />
                  </div>
                  <div className='col-12'>
                      <p className='pt-1' style={{fontSize: '18px'}}><i>Detail: {description}</i></p>
                      <p style={{fontSize: '18px'}}><i>Price: ${price}</i></p>
                      <p style={{fontSize: '18px'}}><i>In stock: {quantity}</i></p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                {/* <button type="button" className="btn btn-primary">Save</button> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}




