import React, { Component } from 'react'
import { products } from './productsData'
import ProductList from './ProductList'
import ModalDetail from './ModalDetail'
import ModalCart from './ModalCart'

export default class ShoesStore extends Component {
    state = {
      productData: products,
      productDetail: products[0],
      productCart: [],
    }
    handleViewDetail= (data) => {
      return this.setState({productDetail: data})
    }
    handleAddToCart = (data) => {
      let clonedCart = [...this.state.productCart];
      let product = this.state.productCart.findIndex((item) => {
        return item.id === data.id;
      });
      // if there is no item in cart => push item to cart; Else add number + 1
      if (product === -1){
        // as cart does not have number/quantity yet => add it into cart and call cart as new name
        let cartItem = {...data, number: 1};
        clonedCart.push(cartItem);
      }else{
        clonedCart[product].number++;
      }
      // update state
      this.setState({
        productCart: clonedCart
      })
    }
    handleAddQuantity = (idSelected) => {
      let clonedCart = [...this.state.productCart];
      let product = clonedCart.findIndex((item) => {
        return item.id === idSelected;
      });
      clonedCart[product].number++;  
      this.setState({
        productCart: clonedCart
      })  
    }

    handleSubQuantity = (idSelected) => {
      let clonedCart = [...this.state.productCart];
      let product = clonedCart.findIndex((item) => {
        return item.id === idSelected;
      });
      if (clonedCart[product].number > 1){
        clonedCart[product].number--;  
        this.setState({
          productCart: clonedCart
        })  
      }
    }
  

  render() {
    let totalQuantity = this.state.productCart.reduce((totalQty,item,index) => {
      return totalQty += item.number
    },0)

    let totalCost = this.state.productCart.reduce ((totalFee,item,index) => {
      return totalFee += item.number * item.price
    },0)

    return (
      <div className='container'>
        <h3 className='py-2' style={{backgroundColor:'#F3EFE0', fontSize: '40px'}}>Shoes Shop</h3>
        {/* shoes cart */}
        <ModalCart addQty={this.handleAddQuantity} subQty={this.handleSubQuantity} productCart={this.state.productCart} totalCost={totalCost}></ModalCart>
        <div className='text-right'><span className='text-danger' style={{cursor: 'pointer', fontSize: '18px', fontWeight: 'bold'}} data-toggle="modal" data-target="#modelCartId">Cart ( {totalQuantity} )</span></div>
        {/* list shoes */}
        <ProductList productList={this.state.productData} viewDetail={this.handleViewDetail} addToCart={this.handleAddToCart}></ProductList>
        {/* shoes detail */}
        <ModalDetail shoesInfo={this.state.productDetail}></ModalDetail>

      </div>
    )
  }
}
