import React, { Component } from 'react'

export default class ProductItem extends Component {
  render() {
    // OOP let {object} = dataShoes defined in parent class for <ShoesItem> component
    let {image,name} = this.props.dataProduct;
    return (
        // Create this div to make spacing between card using padding-1
        <div className='col-3 p-1'>
            {/* create below card from b4-card-align command */}
            <div className="card text-left">
                <img className="card-img-top" src={image} alt="" />
                <div className="card-body">
                    <h6 className="card-title">{name}</h6>
                    <button 
                        onClick={()=>{
                            this.props.cartButton(this.props.dataProduct)
                        }} className='btn btn-success' data-toggle="modal" data-target="#modelCartId">Add to Cart</button>
                    <button 
                        onClick={()=>{
                            this.props.detailButton(this.props.dataProduct)
                        }} className='btn btn-primary ml-2' data-toggle="modal" data-target="#modelId">Detail</button>
                </div>
            </div>
        </div>
    )
  }
}
