import React, { Component } from 'react'

export default class ModalCart extends Component {
    renderTBody= () => {
        return this.props.productCart.map((item) => {
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                {/* set price = price * quantity */}
                <td>${item.price * item.number}</td>
                <td className='d-flex justify-content-center'>
                    <div onClick={()=>{this.props.subQty(item.id)}} className='subQty'>-</div>
                    {item.number}
                <div onClick={()=>{this.props.addQty(item.id)}} className='addQty'>+</div>
                </td>
                <td>
                    <img style={{width: '80px'}} src={item.image} alt="" />
                </td>
            </tr>
        })
    }
  render() {
    return (
        <div>
            {/* Modal */}
            <div className="modal fade" id="modelCartId" tabIndex={-1} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{width: '800px', maxWidth: '800px'}}>
                        <div className="modal-header">
                            <h5 className="modal-title" style={{fontWeight: 'bold', fontSize: '30px'}}>PRODUCT CART</h5>
                            <button type="button" className="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <table className='table'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderTBody()}
                                </tbody>
                                <tfoot>
                                    <tr className='text-danger' style={{fontSize: '20px', fontWeight: 'bold'}}>
                                        <td colSpan='1'></td>
                                        <td>Total Cost:</td>
                                        <td>${this.props.totalCost}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">Purchase</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
