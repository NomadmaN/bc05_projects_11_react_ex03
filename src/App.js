import logo from './logo.svg';
import './App.css';
import ShoesStore from './ShoesStore/ShoesStore';

function App() {
  return (
    <div className="App">
      <ShoesStore></ShoesStore>
    </div>
  );
}

export default App;
